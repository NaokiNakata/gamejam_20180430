﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageController : MonoBehaviour {
	private GameManager _gameManager;

	// Use this for initialization
	void Start () {
		GameObject gameManager = GameObject.Find("Manager/GameManager");
		_gameManager = gameManager.GetComponent<GameManager>();
		if(_gameManager == null){
			Debug.Log("Not Found: GameManager.");
			Application.Quit();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other) {
		GameObject colObj = other.gameObject;
	 	if(colObj.tag == "Ball"){
			Destroy(colObj.gameObject);
			_gameManager.outBall();
		}
	}
}
