﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public enum Mode {
		Normal,
		Penetration,
		ShockWave,
		Shield
	}

	public enum Skill{
		Penetration,
		ShockWave,
		Shield,
		LifeUp
	}

	public float speed = 0.4f;
	public const int ppMax = 10;
	public const int stockMax = 5;
	public Material[] materials; 

	private Rigidbody _rig;
	private int _powerPoint = 0; // PP
	private int _stock = 3; // 残機
	private Mode _mode = Mode.Normal;
	private Dictionary<Skill, int> _skillPointList = new Dictionary<Skill, int>() {
		{Skill.Penetration, 5},
		{Skill.ShockWave, 3},
		{Skill.Shield, 2},
		{Skill.LifeUp, 10}
	};

	// Use this for initialization
	void Start () {
		Debug.Log(_mode);
		_rig = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		_rig.velocity = new Vector2(0, 0);

		if(Input.GetKey(KeyCode.LeftArrow)){
			Vector3 curPos = GetComponent<Rigidbody>().transform.position;
			GetComponent<Rigidbody>().transform.position = new Vector3(curPos.x-speed, curPos.y, curPos.z);

		}else if(Input.GetKey(KeyCode.RightArrow)){
			Vector3 curPos = GetComponent<Rigidbody>().transform.position;
			GetComponent<Rigidbody>().position = new Vector3(curPos.x+speed, curPos.y, curPos.z);

		}
		
		if(Input.GetKey(KeyCode.Space)){
			Bash();

		}else if(Input.GetKey(KeyCode.A)){
			activateSkill(Skill.Penetration);

		}else if(Input.GetKey(KeyCode.S)){
			activateSkill(Skill.ShockWave);

		}else if(Input.GetKey(KeyCode.D)){
			activateSkill(Skill.Shield);

		}else if(Input.GetKey(KeyCode.F)){
			activateSkill(Skill.LifeUp);
		}
	}

	// タイミングよくボールを弾き返す
	void Bash(){
		Debug.Log("Bash");

		// エフェクトを表示

		// ボールに力を加える
	}

	// 必殺技
	public void activateSkill(Skill skill){
		// スキルが使用不可(PP不足or他スキル使用中)の場合は戻る
		if(!checkPowerPoint(skill) || _mode != Mode.Normal){
			Debug.Log("Can't use skill: " + skill + ", your point: " + _powerPoint);
			return;
		}

		switch(skill){
			// 貫通スキル
			case Skill.Penetration:
				updatePlayerMode(Mode.Penetration);
				break;

			// 衝撃波スキル
			case Skill.ShockWave:
				updatePlayerMode(Mode.ShockWave);
				break;

			// シールドスキル
			case Skill.Shield:
				updatePlayerMode(Mode.Shield);
				// シールド状態にする
				skillShield();
				break;
			
			// 残機回復スキル
			case Skill.LifeUp:
				// 残機回復
				skillRecoverStock();
				break;
		}
		
		Debug.Log("Skill: " + skill);
		// PPを消費する
		_powerPoint -= _skillPointList[skill];
	}

	// Skillが使用可能か判定する
	bool checkPowerPoint(Skill skill){
		if(_skillPointList[skill] > _powerPoint){
			return false;
		}else if(_stock >= stockMax){
			return false;
		}

		return true;
	}

	// PP回復
	public void recoverPowerPoint(){
		if(_powerPoint < ppMax){
			_powerPoint+=1;
		}
		Debug.Log("Recover PP: " + _powerPoint);
		return;
	}

	// Stock回復
	public void skillRecoverStock(){
		if(_stock < stockMax ){
			_stock++;
		}
		Debug.Log("Recover Stock: " + _stock);
		return;
	}

	// シールドモード
	public void skillShield(){
		Debug.Log("Shield");
		Vector3 scale = this.transform.localScale;
		this.transform.localScale = new Vector3(scale.x*2.0f, scale.y, scale.z);
	}

	// 残機を減らす
	public bool decreaseStock(){
		_stock--;
		if(_stock==0){
			return false;
		}else{
			return true;
		}
	}

	public Mode getPlayerMode(){
		return _mode;
	}

	public int getPP(){
		return _powerPoint;
	}

	public int getStock(){
		return _stock;
	}

	public void updatePlayerMode(Mode mode){
		// シールドモードから変わる場合は先にサイズを戻す
		if(_mode == Mode.Shield){
			Vector3 scale = this.transform.localScale;
			this.transform.localScale = new Vector3(scale.x/2.0f, scale.y, scale.z);
		}

		_mode = mode;
		this.GetComponent<Renderer>().material = materials[(int)_mode]; 
	}
}
