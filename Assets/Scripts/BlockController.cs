﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour {

	private GameManager _gameManager;

	// Use this for initialization
	void Start () {
		GameObject gameManager = GameObject.Find("Manager/GameManager");
		_gameManager = gameManager.GetComponent<GameManager>();
		if(_gameManager == null){
			Debug.Log("Not Found: GameManager.");
			Application.Quit();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// 衝突時の処理
	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Ball")
		{
			foreach (ContactPoint point in collision.contacts)
			{
				_gameManager.destroyBlock(); // ブロック破壊時の処理呼び出し
				Destroy(this.gameObject);
			}
		}
	}

	private void OnTriggerEnter(Collider other) {
	// 	Debug.Log("Collision detect");
	// 	_gameManager.destroyBlock(); // ブロック破壊時の処理呼び出し
	// 	Destroy(this.gameObject);

	// 	GameObject colObj = other.gameObject;
	// 	if(colObj.tag == "Ball"){
	// 		// 貫通モードじゃない場合は弾く
	// 		if(_gameManager.getBallMode() != BallController.Mode.Penetration ){
	// 			Rigidbody othRig = other.GetComponent<Rigidbody>();
	// 			Debug.Log("V:" + othRig.velocity.y);
	// 			if(othRig.velocity.y > 0){
	// 				othRig.AddForce(0, -200f, 0);
	// 			}else{
	// 				othRig.AddForce(0, 200f, 0);
	// 			}
	// 		}
	// 	}
	}
}
