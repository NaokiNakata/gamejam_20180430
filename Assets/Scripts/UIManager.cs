﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
	public Text gameClearText;
	public Text gameOverText; //ゲームオーバーの文字
	public Text message; //ゲームオーバー時のメッセージ
	public Text ui_stock;
	public Text ui_pp;

	// Use this for initialization
	void Start () {
		gameClearText.enabled = false;
		gameOverText.enabled = false;
		message.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void gameClear(){
		gameClearText.enabled = true;
	}
	public void gameOver(){
		gameOverText.enabled = true;
		message.enabled = true;
	}

	public void updateStatus(int pp, int stock){
		ui_stock.text = "Stock: " + stock;
		ui_pp.text = "PP: " + pp;
	}
}
