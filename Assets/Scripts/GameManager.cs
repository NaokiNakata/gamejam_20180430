﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

	private PlayerController _playerController;
	private BallController _ballController;
	private UIManager _uiManager;
	private int  _restBlocks = 0;
	private bool _isGameOver = false;

	// Use this for initialization
	void Start () {
		// ブロックを配置
		setBlocks(); 

		GameObject player = GameObject.Find("Player");
		_playerController = player.GetComponent<PlayerController>();
		if(_playerController == null){
			Debug.Log("Not Found: Player Controller.");
			Application.Quit();
		}

		GameObject ball = GameObject.Find("Ball");
		_ballController = ball.GetComponent<BallController>();
		if(_ballController == null){
			Debug.Log("Not Found: Ball Controller.");
			Application.Quit();
		}

		GameObject uiManager = GameObject.Find("UIManager");
		_uiManager = uiManager.GetComponent<UIManager>();
		if(_uiManager == null){
			Debug.Log("Not Found: UIManager.");
			Application.Quit();
		}
	}
	
	// Update is called once per frame
	void Update () {
		_uiManager.updateStatus(_playerController.getPP(), _playerController.getStock());
		if(_isGameOver){
			//ゲームオーバーの文字を表示
			_uiManager.gameOver();
			if(Input.GetKey(KeyCode.R)){
				Application.LoadLevel(Application.loadedLevelName);
			}
		}
	}

	void setBlocks(){
		GameObject blockPrefab = (GameObject)Resources.Load("Prefabs/Block");
		if(blockPrefab == null){
			Debug.Log("Not Found: Block Prefab.");
			Application.Quit();
		}

		const float width = 1.0f; // ブロックの幅
		const float shift = 0.5f; // 1行ごとのずらし
		const float gap = 0.6f; // 各行の隙間
		const float limUp = 4.7f, limDown = 2.7f;
		const float limLeft = -8.3f, limRight = 8.3f;
		
		bool isShift = true;
		for (float y = limDown; y < limUp; y+=gap){
			float x0, xlim;
			if(isShift){
				x0 = limLeft;
				xlim = limRight;
			}else{
				x0 = limLeft + shift;
				xlim = limRight - shift;
			}

			for (float x = x0; x < xlim; x += width){
				Vector3 pos = new Vector3(x, y, 0.0f);
				Instantiate (blockPrefab, pos, Quaternion.identity);
				_restBlocks++;
			}
			isShift = !isShift; // スイッチング
		}
	}

	public void destroyBlock(){
		_restBlocks--;

		// ブロックが0個ならクリア
		if(_restBlocks == 0){
			Debug.Log("Clear.");
			_uiManager.gameClear();
			Application.Quit();
		}

		// ボールが通常状態ならPPを回復
		if(_ballController.getMode() == BallController.Mode.Normal){
			_playerController.recoverPowerPoint();
		}
	}

	// プレイヤーとボールの衝突時に呼び出し
	public void  playerBallCollision(){
		// ボールの状態を戻す
		_ballController.updateMode(BallController.Mode.Normal);

		// スキルの実行を判定
		PlayerController.Mode playerMode = _playerController.getPlayerMode();
		if(playerMode == PlayerController.Mode.Penetration){
			//(メソッド,秒数)を指定
    		Invoke("delayPenetrate", 0.2f);

		}else if(playerMode == PlayerController.Mode.ShockWave){
			// ボールの状態を衝撃波モードに変更
			_ballController.updateMode(BallController.Mode.ShockWave);
			// プレイヤーの状態をNormalに変更
			_playerController.updatePlayerMode(PlayerController.Mode.Normal);

		}else if(playerMode == PlayerController.Mode.Shield){
			// プレイヤーの状態をNormalに変更
			_playerController.updatePlayerMode(PlayerController.Mode.Normal);

		}
	}

	public BallController.Mode getBallMode(){
		return _ballController.getMode();
	}

	// 貫通モード用の処理
	private void delayPenetrate(){
		// ボールの状態を貫通モードに変更
		_ballController.updateMode(BallController.Mode.Penetration);
		// プレイヤーの状態をNormalに変更
		_playerController.updatePlayerMode(PlayerController.Mode.Normal);
	}

	public void outBall(){
		// 残機を減らし継続不能な場合終了
		bool isContinuable = _playerController.decreaseStock();
		if(isContinuable){
			// 新たにボールを生成
			GameObject ballPrefab = (GameObject)Resources.Load("Prefabs/Ball");
			Vector3 pos = new Vector3(0.0f, 2.0f, 0.0f);
			GameObject ball = Instantiate (ballPrefab, pos, Quaternion.identity) as GameObject;
			_ballController = ball.GetComponent<BallController>();
			if(_ballController == null){
				Debug.Log("Not Found: Ball Controller.");
				// Application.Quit();
			}
			_ballController.init();
		}else{
			_isGameOver = true;
		}
	}
}
