﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

	public Material[] materials;
	private float bouncePower = 200.0f;
	private float explosionRadius = 1.2f;

	private Mode _mode = Mode.Normal;
	private GameManager _gameManager;
	private Rigidbody _rig;

	public enum Mode {
		Normal,
		Penetration,
		ShockWave
	}

	// Use this for initialization
	void Start () {
		init();
	}

	public void init(){
		GameObject gameManager = GameObject.Find("Manager/GameManager");
		_gameManager = gameManager.GetComponent<GameManager>();
		if(_gameManager == null){
			Debug.Log("Not Found: GameManager.");
			Application.Quit();
		}

		_rig = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		// ボールに初速を与える
		if(Input.GetButtonUp("Jump") && _rig.velocity == new Vector3(0, 0, 0)){
			float v_x0 = Random.Range(-200.0f, 200.0f);
			_rig.AddForce(v_x0, -bouncePower, 0);
		}
	}

	// 衝突時の処理
	void OnCollisionEnter(Collision collision)
	{
		GameObject colObj = collision.gameObject;
		if (colObj.tag == "Player"){
			foreach (ContactPoint point in collision.contacts)
			{
				//衝突位置
				Vector3 collisionPoint = new Vector3(
					(point.point.x - transform.position.x), 
					(point.point.y - transform.position.y),
					(point.point.z - transform.position.z)
				);
				Debug.Log("CollisionPoint : " + collisionPoint);

				// 衝突時のスキル判定処理
				_gameManager.playerBallCollision();

				// 反射する
				Rigidbody rig = GetComponent<Rigidbody>();
				rig.AddForce(0, bouncePower, 0);

				// 値が小さすぎる場合補正
				if (Mathf.Abs(rig.velocity.y) < 2) {
					rig.velocity = new Vector2(rig.velocity.x, rig.velocity.y*2);
				}
				if (Mathf.Abs(rig.velocity.x) < 2) {
					rig.velocity = new Vector2(rig.velocity.x*2, rig.velocity.y);
				}
			}
		}else if(colObj.tag == "Block"){
			// 衝撃波モードの場合
			if(_mode == Mode.ShockWave){
				shockWave();
				updateMode(Mode.Normal);
			}
		}
	}

	// 貫通モードの衝突判定
	private void OnTriggerEnter(Collider other) {
		GameObject colObj = other.gameObject;

		// 貫通モードの場合は通り抜ける
		if(_mode == Mode.Penetration && colObj.tag=="Block"){
			_gameManager.destroyBlock();
			Destroy(other.gameObject);

		// それ以外は反射
		}else{
			Debug.Log("Reflect");
			Rigidbody rig = GetComponent<Rigidbody>();
			if(colObj.tag == "SideWall"){
				rig.velocity= new Vector3(-rig.velocity.x, rig.velocity.y, 0);
			}else if(colObj.tag == "Player" && _rig.velocity.y < 0){
				updateMode(Mode.Normal);
				rig.velocity= new Vector3(rig.velocity.x, -rig.velocity.y, 0);
			}else{
				rig.velocity= new Vector3(rig.velocity.x, -rig.velocity.y, 0);
			}
		}
	}

	public Mode getMode(){
		return _mode;
	}

	public void updateMode(Mode mode){
		_mode = mode;

		Collider col = this.gameObject.GetComponent<Collider>();
		if(_mode == Mode.Penetration){
			Debug.Log("Change isTrigger: true");
			col.isTrigger = true;
		}else{
			col.isTrigger = false;
		}
		Debug.Log("Ball mode: "+_mode);

		this.GetComponent<Renderer>().material = materials[(int)_mode]; 
	}

	// 衝撃波を発生させる
	private void shockWave(){
		Collider[] targets = Physics.OverlapSphere(transform.position,explosionRadius);
		// 衝撃波の範囲内のオブジェクトを破壊
		foreach (Collider obj in targets) {
			if ((obj)&&(obj.tag=="Block")) {
				Destroy(obj.gameObject);
				_gameManager.destroyBlock();
			}
		}
	}
}
